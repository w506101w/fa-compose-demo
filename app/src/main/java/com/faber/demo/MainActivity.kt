package com.faber.demo

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.faber.demo.ui.screen.navigation.NavigationPage
import com.faber.demo.ui.theme.FaComposeDemoTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            FaComposeDemoTheme {
                NavigationPage()
            }
        }
    }
}
