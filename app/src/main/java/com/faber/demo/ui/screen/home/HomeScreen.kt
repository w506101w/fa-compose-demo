package com.faber.demo.ui.screen.home

import android.util.Log
import androidx.annotation.StringRes
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.List
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Send
import androidx.compose.material3.ListItem
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import com.faber.demo.R
import com.faber.demo.ui.components.AnimatedNavigation
import com.faber.demo.ui.screen.demo.jetpack.demo01.MessageCard
import com.faber.demo.ui.screen.destinations.DirectionDestination
import com.faber.demo.ui.screen.destinations.HomeScreenDestination
import com.faber.demo.ui.screen.destinations.JetpackDemo01ViewDestination
import com.faber.demo.ui.screen.destinations.JetpackDemo02ViewDestination
import com.faber.demo.ui.screen.destinations.JetpackDemo03ViewDestination
import com.faber.demo.ui.screen.destinations.JetpackDemo04ViewDestination
import com.faber.demo.ui.screen.destinations.JetpackDemo05ViewDestination
import com.faber.demo.ui.screen.destinations.JetpackDemo06ViewDestination
import com.faber.demo.ui.screen.destinations.JetpackDemo07ViewDestination
import com.faber.demo.ui.screen.destinations.JetpackDemo08ViewDestination
import com.faber.demo.ui.screen.destinations.JetpackDemo09ViewDestination
import com.faber.demo.ui.screen.destinations.MineScreenDestination
import com.faber.demo.ui.screen.destinations.NewsScreenDestination
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.annotation.RootNavGraph
import com.ramcosta.composedestinations.navigation.DestinationsNavigator

@RootNavGraph(start = true)
@Destination
@Composable
fun HomeScreen(
    navigator: DestinationsNavigator
) {
    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        LazyColumn {
            items(HomeDemoItem.values()) {
                ListItem(
                    modifier = Modifier.clickable {
                        navigator.navigate(it.direction)
                    },
                    headlineContent = { Text(it.title)}
                )
            }
        }
    }
}

enum class HomeDemoItem(
    val direction: DirectionDestination,
    val title: String
) {
    JetpackDemo01View(JetpackDemo01ViewDestination, "JetpackDemo01View Message"),
    JetpackDemo02View(JetpackDemo02ViewDestination, "JetpackDemo02View MessageCardExpandable"),
    JetpackDemo03View(JetpackDemo03ViewDestination, "JetpackDemo03View ScaffoldExample"),
    JetpackDemo04View(JetpackDemo04ViewDestination, "JetpackDemo04View Material3Example"),
    JetpackDemo05View(JetpackDemo05ViewDestination, "JetpackDemo05View Navigation Drawer"),
    JetpackDemo06View(JetpackDemo06ViewDestination, "JetpackDemo06View Snackbar Demo"),
    JetpackDemo07View(JetpackDemo07ViewDestination, "JetpackDemo07View List And Grid"),
    JetpackDemo08View(JetpackDemo08ViewDestination, "JetpackDemo08View Canvas"),
    JetpackDemo09View(JetpackDemo09ViewDestination, "JetpackDemo09 Image Picker"),
}

//@Preview(showBackground = true)
//@Composable
//fun HomeViewPreview() {
//    FaComposeDemoTheme {
//        HomeView()
//    }
//}

