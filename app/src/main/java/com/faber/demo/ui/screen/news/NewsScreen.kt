package com.faber.demo.ui.screen.news

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import com.ramcosta.composedestinations.annotation.Destination

@Destination
@Composable
fun NewsScreen() {
    Text(text = "News")
}