package com.faber.demo.ui.screen.demo.jetpack.demo01

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.ListItem
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.faber.demo.ui.screen.destinations.DirectionDestination
import com.faber.demo.ui.screen.destinations.JetpackCanvasDemo01ViewDestination
import com.faber.demo.ui.screen.destinations.JetpackCanvasDemo02ViewDestination
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator


@Destination
@Composable
fun JetpackDemo08View(
    navigator: DestinationsNavigator
) {
    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        LazyColumn {
            items(CanvasDemoItem.values()) {
                ListItem(
                    modifier = Modifier.clickable {
                        navigator.navigate(it.direction)
                    },
                    headlineContent = { Text(it.title)}
                )
            }
        }
    }
}

enum class CanvasDemoItem(
    val direction: DirectionDestination,
    val title: String
) {
    JetpackCanvasDemo01View(JetpackCanvasDemo01ViewDestination, "Canvas Basic transformations"),
    JetpackCanvasDemo02View(JetpackCanvasDemo02ViewDestination, "Common drawing operations"),
}


